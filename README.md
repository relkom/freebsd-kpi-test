#FreeBSD 13.1 Rust KPI test suite

This repo contains a code which is used to verify that the `ffi` layer was designed correctly. 

In directory `C` there is a program which generates a Rust program which contains a test code.

In `main.c` a structutres to compare are defined using macroses.


In directory `src` a generated code is palced.

By calling `cargo test` a test of the struct, enum, union sizes and alignment is tested.

